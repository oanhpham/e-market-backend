#!/bin/bash

./gradlew build

./gradlew --stop

java -jar build/libs/e-market-0.0.1-SNAPSHOT.jar &
echo $! > pid
