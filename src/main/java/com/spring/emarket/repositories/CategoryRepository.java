package com.spring.emarket.repositories;

import com.spring.emarket.commons.Status;
import com.spring.emarket.models.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    Category findByIdAndStatus(Long id, @NotNull @NotEmpty Status status);

}
