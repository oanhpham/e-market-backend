package com.spring.emarket.repositories;

import com.spring.emarket.commons.Status;
import com.spring.emarket.models.entities.Promotion;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public interface PromotionRepository extends JpaRepository<Promotion, Long> {

    Promotion findByIdAndIsDelete(Long id, Byte isDelete);

}
