package com.spring.emarket.repositories;

import com.spring.emarket.commons.Status;
import com.spring.emarket.models.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {

    Product findByIdAndStatus(Long id, @NotNull @NotEmpty Status status);


    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "Update products set status = 'TERMINATE' where id = ?1")
    void deleteById(Long id);

    @Query (value = "select *" +
            "from products o " +
            "where o.created_at BETWEEN (SUBDATE(now(), weekday(now()))) AND (SUBDATE(now(),weekday(now()) -6))", nativeQuery = true)
    List<Product> getProducts();

}
