package com.spring.emarket.repositories;

import com.spring.emarket.models.entities.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ImageRepository extends JpaRepository<Image,Long> {

    List<Image> findByProductId(long idProduct);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "Update images set product_id = 0 where product_id = ?1")
    void deleteRelationshipProduct(long idProduct);

}
