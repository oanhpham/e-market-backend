package com.spring.emarket.repositories.dao;

import com.spring.emarket.commons.DataUtil;
import com.spring.emarket.commons.Status;
import com.spring.emarket.models.entities.Image;
import com.spring.emarket.models.entities.Product;
import com.spring.emarket.models.response.FormPaging;
import com.spring.emarket.models.response.ProductDTO;
import com.spring.emarket.repositories.ImageRepository;
import com.spring.emarket.services.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ProductMapper mapper;

    @Autowired
    private ImageRepository imageRepository;

    public FormPaging<?> filter(String name, int id, Double costStart, Double costEnd, int categoryId, int promotionId, int page, int size) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> cq = cb.createQuery(Product.class);
        CriteriaQuery<Long> cq1 = cb.createQuery(Long.class);
        Root<Product> promotion = cq.from(Product.class);

        cq.select(promotion);
        cq1.select(cb.count(cq1.from(Product.class)));
        List<Predicate> predicates = new ArrayList<>();

        // get only status == ACTIVE
        predicates.add(cb.equal(promotion.get("status"), Status.ACTIVE));

        // // filter with id exist
        if (id > 0) {
            predicates.add(cb.equal(promotion.get("id"), id));
        }

        // filter with costStart exist
        if (costStart > 0) {
            predicates.add(cb.greaterThanOrEqualTo(promotion.get("cost"), costStart));
        }

        // filter with costEnd exist
        if (costEnd > 0) {
            predicates.add(cb.lessThanOrEqualTo(promotion.get("cost"), costEnd));
        }

        // filter with categoryId exist
        if (categoryId > 0) {
            predicates.add(cb.equal(promotion.get("categoryId"), categoryId));
        }

        // filter with promotionID exist
        if (promotionId > 0) {
            predicates.add(cb.equal(promotion.get("promotionId"), promotionId));
        }

        // filter with name exist
        if (!DataUtil.isNullOrEmpty(name)) {
            predicates.add(cb.or(cb.like(cb.lower(promotion.get("name")), "%" + name.trim().toLowerCase() + "%")));
        }

        cq.where(predicates.toArray(new Predicate[0]));

        int startIndex = page * size;

        List<ProductDTO> promotionDTOS = entityManager.createQuery(cq)
                .setFirstResult(startIndex)
                .setMaxResults(size)
                .getResultList()
                .stream()
                .map(product -> mapper.map(product))
                .peek(it -> {
                    List<Image> images = imageRepository.findByProductId(it.getId());
                    it.setImages(images);
                })
                .collect(Collectors.toList());

        cq1.where(predicates.toArray(new Predicate[0]));
        Long totalElement = entityManager.createQuery(cq1).getSingleResult();

        int totalPage = (int) Math.ceil((double) totalElement / size);

        return paging(page, size, totalPage, promotionDTOS.size(), totalElement.intValue(), promotionDTOS);
    }

    private FormPaging<?> paging(int page, int size, int totalPage, int numberRecord, int totalRecord, List<ProductDTO> data){
        FormPaging<List<ProductDTO>> result = new FormPaging<>();

        result.setPage(page);
        result.setSize(size);
        result.setTotalPage(totalPage);
        result.setNumberRecordPage(numberRecord);
        result.setData(data);
        result.setTotalRecord(totalRecord);

        return result;
    }

}
