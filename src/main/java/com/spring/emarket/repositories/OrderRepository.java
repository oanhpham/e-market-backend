package com.spring.emarket.repositories;

import com.spring.emarket.models.entities.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderRepository extends JpaRepository<Cart, Long> {

    Cart findByUserIdAndIsDeleted(Long userId, Byte isDelete);

    @Query(value = "select *" +
            "from cart o " +
            "where o.pay_status >=1 and o.pay_status <= 2 and o.user_id = ?1", nativeQuery = true)
    List<Cart> findCartHistory(Long userId);

    Cart findCartByPayStatusAndUserId(Byte payType, Long userId);

    @Query (value = "select count(o.id)" +
            "from cart o " +
            "where o.start_date BETWEEN (SUBDATE(now(), weekday(now()))) AND (SUBDATE(now(),weekday(now()) -6))", nativeQuery = true)
    Long countCartInWeek ();

    @Query (value = "select count(o.id) from cart o where month(o.start_date) = MONTH(now())", nativeQuery = true)
    Long countCartInMonth ();

    Long countCartByPayStatus(Byte status);

    List<Cart> findAllByPayStatus(Byte status);
}
