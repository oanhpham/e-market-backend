package com.spring.emarket.repositories;

import com.spring.emarket.commons.Status;
import com.spring.emarket.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);

    Optional<User> findById(Long id);

    Boolean existsByEmail(String email);

    Long countUserByStatus(Status status);

    List<User> getAllByStatus(Status status);

}
