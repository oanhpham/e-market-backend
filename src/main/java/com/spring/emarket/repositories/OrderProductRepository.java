package com.spring.emarket.repositories;

import com.spring.emarket.models.entities.OrderProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderProductRepository extends JpaRepository<OrderProduct, Long> {

    OrderProduct findByOrderIdAndProductIdAndIsDelete(Long orderId, Long productId, Byte status);

    OrderProduct findByUserIdAndProductIdAndIsDelete(Long userId, Long productId, Byte status);

    List<OrderProduct> findByUserIdAndIsDelete(Long userId,Byte isDelete);

    List<OrderProduct> findByOrderIdAndIsDelete(Long orderId, Byte isDeleted);
}
