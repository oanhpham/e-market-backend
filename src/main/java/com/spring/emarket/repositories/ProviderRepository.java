package com.spring.emarket.repositories;

import com.spring.emarket.models.entities.Provider;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProviderRepository extends JpaRepository<Provider, Long> {

    Provider findProviderByIdAndIsDelete(Long providerId, Byte isDelete);
}
