package com.spring.emarket.repositories;

import com.spring.emarket.commons.Status;
import com.spring.emarket.models.entities.UnitProduct;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public interface UnitProductRepository extends JpaRepository<UnitProduct, Long> {
}
