package com.spring.emarket.controller;

import com.spring.emarket.models.response.ProductDTO;
import com.spring.emarket.security.CurrentUser;
import com.spring.emarket.security.UserPrincipal;
import com.spring.emarket.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductService service;

    @PostMapping
    ResponseEntity<?> create(@CurrentUser UserPrincipal userPrincipal,
                             @Valid @RequestBody ProductDTO productDTO) {
        return service.create(productDTO, userPrincipal);
    }

    @PutMapping
    ResponseEntity<?> update(@CurrentUser UserPrincipal userPrincipal,
                             @Valid @RequestBody ProductDTO productDTO) {
        return service.update(productDTO, userPrincipal);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<?> delete(@CurrentUser UserPrincipal userPrincipal,
                             @PathVariable Long id) {
        return service.delete(id);
    }

    @GetMapping
    ResponseEntity<?> product(@RequestParam(value = "name", required = false) String name,
                              @RequestParam(value = "id", defaultValue = "0") int id,
                              @RequestParam(value = "cost-start", defaultValue = "0") Double costStart,
                              @RequestParam(value = "cost-end", defaultValue = "0") Double costEnd,
                              @RequestParam(value = "category-id", defaultValue = "0") int categoryId,
                              @RequestParam(value = "promotion-id", defaultValue = "0") int promotionId,
                              @RequestParam(value = "page", defaultValue = "0") int page,
                              @RequestParam(value = "size", defaultValue = "25") int size) {
        return service.listProduct(name, id, costStart, costEnd, categoryId, promotionId, page, size);
    }

    @PutMapping("/rate")
    ResponseEntity<?> rate(@CurrentUser UserPrincipal userPrincipal,
                           @RequestParam("type_rate") int star,
                           @RequestParam("id-product") long idProduct){
        return service.rate(idProduct, star);
    }
    @GetMapping("/{id}")
    ResponseEntity<?> getDetail(@PathVariable("id") Long id) {
        return service.getDetailProduct(id);
    }

    @GetMapping("/new_arrivals")
    ResponseEntity<?> getNewArrivals() {
        return service.getArrivalsProduct();
    }

}
