package com.spring.emarket.controller;

import com.spring.emarket.models.entities.Cart;
import com.spring.emarket.models.entities.OrderProduct;
import com.spring.emarket.models.request.CartDTO;
import com.spring.emarket.security.CurrentUser;
import com.spring.emarket.security.UserPrincipal;
import com.spring.emarket.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cart")
public class CartController {

    @Autowired
    OrderService orderService;

    @PostMapping
    @PreAuthorize("hasRole('USER')")
    ResponseEntity initial(@CurrentUser UserPrincipal currentUser, @RequestBody Cart cart) {
        cart.setUserId(currentUser.getId());
        return orderService.initial(currentUser.getId(), cart);
    }

    @PutMapping
    @PreAuthorize("hasRole('USER')")
    ResponseEntity updateCart(@CurrentUser UserPrincipal userPrincipal, @RequestBody OrderProduct orderProduct) {
        return orderService.saveOrderProduct(userPrincipal.getId(), orderProduct);
    }

    @PutMapping("/detail-info")
    @PreAuthorize(("hasRole('USER')"))
    ResponseEntity updateDetail(@CurrentUser UserPrincipal userPrincipal, @RequestBody CartDTO cartDTO) {
        return orderService.updateCart(userPrincipal.getId
(), cartDTO);
    }

    @GetMapping
    ResponseEntity getDetailCart(@CurrentUser UserPrincipal userPrincipal) {
        return orderService.getDetailCart(userPrincipal.getId());
    }
    @GetMapping("/cart-history")
    ResponseEntity getCartHistory(@CurrentUser UserPrincipal userPrincipal) {
        return orderService.getHistoryCart(userPrincipal.getId());
    }


    @PutMapping("/remove-order")
    @PreAuthorize(("hasRole('USER')"))
    ResponseEntity removeProductFromCart(@CurrentUser UserPrincipal userPrincipal, @RequestBody OrderProduct orderProduct) {
        return orderService.removeProductFromCart(userPrincipal.getId(), orderProduct);
    }

    @PutMapping("/processing-order")
    @PreAuthorize(("hasRole('USER')"))
    ResponseEntity processingMode(@CurrentUser UserPrincipal userPrincipal) {
        return orderService.processingOrder(userPrincipal.getId());
    }

    @PutMapping("/shipping-order/{id}")
    @PreAuthorize(("hasRole('USER')"))
    ResponseEntity shippingMode(@CurrentUser UserPrincipal userPrincipal, @PathVariable Long id) {
        return orderService.shippingMode(userPrincipal.getId(), id);
    }

    @PutMapping("/cancel-order/{id}")
    @PreAuthorize(("hasRole('USER')"))
    ResponseEntity cancelMode(@CurrentUser UserPrincipal userPrincipal, @PathVariable Long id) {
        return orderService.cancelMode(userPrincipal.getId(), id);
    }

    @PutMapping("/complete-order/{id}")
    @PreAuthorize(("hasRole('USER')"))
    ResponseEntity doneMode(@CurrentUser UserPrincipal userPrincipal, @PathVariable Long id) {
        return orderService.doneMode(userPrincipal.getId(), id);
    }

    @GetMapping("/order-product-history")
    ResponseEntity orderProductHistory(@CurrentUser UserPrincipal userPrincipal) {
        return orderService.getOrderProductIsDelete(userPrincipal.getId());
    }
    @GetMapping("/summary/week")
    ResponseEntity totalCartInWeek() {
        return orderService.totalTransactionInWeek();
    }
    @GetMapping("/summary/month")
    ResponseEntity totalCartInMonth() {
        return orderService.totalTransactionInMonth();
    }
    @GetMapping("/summary/ratio")
    ResponseEntity ratioTransaction() {
        return orderService.ratioTransaction();
    }

    @GetMapping("/processing-mode")
    ResponseEntity getCartInProcessingMode() {
        return orderService.getAllCartInProcess();
    }
    @GetMapping("/shipping-mode")
    ResponseEntity getCartInShippingMode() {
        return orderService.getAllCartShipping();
    }
    @GetMapping("/cancel-mode")
    ResponseEntity getCartInCancelMode() {
        return orderService.cancelMode();
    }
      @GetMapping("/done-mode")
    ResponseEntity getCartInDoneMode() {
        return orderService.getAllCartDone();
    }



}
