package com.spring.emarket.controller;

import com.spring.emarket.commons.SystemResponse;
import com.spring.emarket.models.entities.Promotion;
import com.spring.emarket.models.request.PromotionRequest;
import com.spring.emarket.security.CurrentUser;
import com.spring.emarket.security.UserPrincipal;
import com.spring.emarket.services.PromotionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/promotion")
public class PromotionController {

    @Autowired
    PromotionService promotionService;

    @PostMapping
    SystemResponse create(@CurrentUser UserPrincipal userPrincipal, @Valid @RequestBody PromotionRequest promotionRequest) {
        Promotion promotion = promotionService.create(userPrincipal.getId(), promotionRequest);
        return new SystemResponse(201, "Promotion was created", promotion);
    }

    @PutMapping("/{promotion-id}")
    SystemResponse update(@CurrentUser UserPrincipal userPrincipal,
                       @PathVariable("promotion-id") Long promotionId,
                          @Valid @RequestBody PromotionRequest promotionRequest) {
        Promotion promotion = promotionService.update(userPrincipal.getId(), promotionId, promotionRequest);
        return new SystemResponse(200, "Promotion was update", promotion);
    }

    @GetMapping
    SystemResponse getAll() {
        return new SystemResponse(200, "promotion list", promotionService.listPromotion());
    }


}
