package com.spring.emarket.controller;

import com.spring.emarket.commons.SystemResponse;
import com.spring.emarket.models.entities.UnitProduct;
import com.spring.emarket.services.UnitProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/unit")
public class UnitProductController {

    @Autowired
    UnitProductService unitProductService;

    @PostMapping
    SystemResponse create(@Valid @RequestBody UnitProduct unitProduct) {
        return new SystemResponse(201, "the unit product was create", unitProductService.create(unitProduct));
    }

    @GetMapping
    SystemResponse listAll() {
        return new SystemResponse(200, "units", unitProductService.units());
    }

}
