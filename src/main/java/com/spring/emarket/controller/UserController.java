package com.spring.emarket.controller;

import com.spring.emarket.commons.SystemResponse;
import com.spring.emarket.exception.ResourceNotFoundException;
import com.spring.emarket.models.entities.User;
import com.spring.emarket.models.request.UserRequest;
import com.spring.emarket.repositories.UserRepository;
import com.spring.emarket.security.CurrentUser;
import com.spring.emarket.security.UserPrincipal;
import com.spring.emarket.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    UserService userService;

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public User getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
        return userRepository.findByEmail(userPrincipal.getEmail())
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId()));
    }

    @PutMapping("/user/{id}")
    @PreAuthorize("hasRole('USER')")
    SystemResponse updateInfo (@PathVariable("id") Long id,
                               @Valid @RequestBody UserRequest userRequest) {
        return new SystemResponse(200, "user info was updated successfuly", userService.updateInfo(id, userRequest));
    }

    @GetMapping("/user")
    SystemResponse totalUser() {
        return new SystemResponse(200, "", userService.totalUser());
    }

    @GetMapping("/user/all")
    SystemResponse getAll () {
        return new SystemResponse(200, "All client in using service", userService.getAllUser());
    }
}
