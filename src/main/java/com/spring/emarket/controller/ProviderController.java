package com.spring.emarket.controller;

import com.spring.emarket.repositories.ProviderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/provider")
public class ProviderController {

    @Autowired
    ProviderRepository providerRepository;

    @GetMapping
    public ResponseEntity<?> getAllProvider () {
        return new ResponseEntity<>(providerRepository.findAll(), HttpStatus.OK);
    }

}
