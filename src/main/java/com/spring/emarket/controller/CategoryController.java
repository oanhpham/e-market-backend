package com.spring.emarket.controller;

import com.spring.emarket.commons.SystemResponse;
import com.spring.emarket.models.entities.Category;
import com.spring.emarket.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @PostMapping
    SystemResponse create (@Valid @RequestBody Category category) {
        return new SystemResponse(201, "the category was create", categoryService.create(category));
    }
    @GetMapping("/{id}")
    SystemResponse update (@PathVariable("id") Long id,
                           @Valid @RequestBody Category category) {
        return new SystemResponse(200, "the category "+ id +" was update", categoryService.update(id, category));
    }
    @GetMapping
    SystemResponse getAll () {
        return new SystemResponse(200, "list category", categoryService.getAllCategories());
    }
}
