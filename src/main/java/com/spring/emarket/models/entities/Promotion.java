package com.spring.emarket.models.entities;

import com.spring.emarket.commons.Status;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "promotions", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id")
})
public class Promotion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @NotEmpty
    private String title;

    @NotNull
    @NotEmpty
    private String content;

    @NotNull
    @NotEmpty
    private Double reducePercent;

    private Long minTotalMoneyOder;

    private Long limitMoney;

   private Byte isDelete = 0;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private Integer numberDayFollowTimeRegister;

    @CreationTimestamp
    private LocalDateTime createdAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Double getReducePercent() {
        return reducePercent;
    }

    public void setReducePercent(Double reducePercent) {
        this.reducePercent = reducePercent;
    }

    public Long getMinTotalMoneyOder() {
        return minTotalMoneyOder;
    }

    public void setMinTotalMoneyOder(Long minTotalMoneyOder) {
        this.minTotalMoneyOder = minTotalMoneyOder;
    }

    public Long getLimitMoney() {
        return limitMoney;
    }

    public void setLimitMoney(Long limitMoney) {
        this.limitMoney = limitMoney;
    }

    public Byte getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Byte isDelete) {
        this.isDelete = isDelete;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Integer getNumberDayFollowTimeRegister() {
        return numberDayFollowTimeRegister;
    }

    public void setNumberDayFollowTimeRegister(Integer numberDayFollowTimeRegister) {
        this.numberDayFollowTimeRegister = numberDayFollowTimeRegister;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
