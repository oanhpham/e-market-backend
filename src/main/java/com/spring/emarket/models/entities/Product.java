package com.spring.emarket.models.entities;

import com.spring.emarket.commons.Status;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @NotEmpty
    private String name;

    private String productCode;

    @Column(columnDefinition = "VARCHAR(500)")
    private String description;

    @NotNull
    @NotEmpty
    private Double cost;

    @NotNull
    @NotEmpty
    // giá nhập kho
    private Double inputPrice;

    @NotNull
    @NotEmpty
    private Long categoryId;

    @NotNull
    @NotEmpty
    private Long providerId;

    @NotNull
    @NotEmpty
    private Long unitProductId;

    private Long promotionId;

    private int star;

    private int star2;

    private int star3;

    private int star4;

    private int star5;

    private int quantity;

    @NotNull
    @NotEmpty
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_ACTIVE;

    @NotNull
    @NotEmpty
    private LocalDateTime startDate;

    @NotNull
    @NotEmpty
    private LocalDateTime expiryDate;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    public Long getProviderId() {
        return providerId;
    }

    public void setProviderId(Long providerName) {
        this.providerId = providerName;
    }

    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getUnitProductId() {
        return unitProductId;
    }

    public void setUnitProductId(Long unitProductId) {
        this.unitProductId = unitProductId;
    }

    public Long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Long promotionId) {
        this.promotionId = promotionId;
    }

    public int getstar() {
        return star;
    }

    public void setstar(int star) {
        this.star = star;
    }

    public int getstar2() {
        return star2;
    }

    public void setstar2(int star2) {
        this.star2 = star2;
    }

    public int getstar3() {
        return star3;
    }

    public void setstar3(int star3) {
        this.star3 = star3;
    }

    public int getstar4() {
        return star4;
    }

    public void setstar4(int star4) {
        this.star4 = star4;
    }

    public Double getInputPrice() {
        return inputPrice;
    }

    public void setInputPrice(Double inputPrice) {
        this.inputPrice = inputPrice;
    }

    public int getstar5() {
        return star5;
    }

    public void setstar5(int star5) {
        this.star5 = star5;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", productCode='" + productCode + '\'' +
                ", description='" + description + '\'' +
                ", cost=" + cost +
                ", inputPrice=" + inputPrice +
                ", categoryId=" + categoryId +
                ", categoryId=" + providerId +
                ", unitProductId=" + unitProductId +
                ", promotionId=" + promotionId +
                ", star=" + star +
                ", star2=" + star2 +
                ", star3=" + star3 +
                ", star4=" + star4 +
                ", star5=" + star5 +
                ", status=" + status +
                ", quantity=" + quantity +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
