package com.spring.emarket.models.entities;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "unit_product")
public class UnitProduct{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}