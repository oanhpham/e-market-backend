package com.spring.emarket.models.response;

import com.spring.emarket.commons.Status;
import com.spring.emarket.models.entities.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

public class ProductDTO {

    private Long id;

    @NotNull
    @NotEmpty
    private String name;

    @NotNull
    @NotEmpty
    private String productCode;

    private String description;

    @NotNull
    private Double cost;

    // giá nhập kho
    @NotNull
    private Double inputPrice;

    @NotNull
    private Category category;

    @NotNull
    private UnitProduct unitProduct;

    private Promotion promotion;

    private Provider provider;

    private List<Image> images;

    private int star = 0;

    private int star2 = 0;

    private int star3 = 0;

    private int star4 = 0;

    private int star5 = 0;

    private int quantity = 0;

    private Status status = Status.ACTIVE;

    @NotNull
    @NotEmpty
    private String startDate;

    @NotNull
    @NotEmpty
    private String expiryDate;

    @CreationTimestamp
    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    public ProductDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getInputPrice() {
        return inputPrice;
    }

    public void setInputPrice(Double inputPrice) {
        this.inputPrice = inputPrice;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public UnitProduct getUnitProduct() {
        return unitProduct;
    }

    public void setUnitProduct(UnitProduct unitProduct) {
        this.unitProduct = unitProduct;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    public int getstar() {
        return star;
    }

    public void setstar(int star) {
        this.star = star;
    }

    public int getstar2() {
        return star2;
    }

    public void setstar2(int star2) {
        this.star2 = star2;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public int getstar3() {
        return star3;
    }

    public void setstar3(int star3) {
        this.star3 = star3;
    }

    public int getstar4() {
        return star4;
    }

    public void setstar4(int star4) {
        this.star4 = star4;
    }

    public int getstar5() {
        return star5;
    }

    public void setstar5(int star5) {
        this.star5 = star5;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "ProductDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", productCode='" + productCode + '\'' +
                ", description='" + description + '\'' +
                ", cost=" + cost +
                ", inputPrice=" + inputPrice +
                ", category=" + category +
                ", unitProduct=" + unitProduct +
                ", promotion=" + promotion +
                ", star=" + star +
                ", star2=" + star2 +
                ", star3=" + star3 +
                ", star4=" + star4 +
                ", star5=" + star5 +
                ", quantity="+ quantity +
                ", status=" + status +
                ", startDate=" + startDate +
                ", expiryDate=" + expiryDate +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
