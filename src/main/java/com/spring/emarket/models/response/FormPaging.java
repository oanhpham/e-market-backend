package com.spring.emarket.models.response;

public class FormPaging<T> {

    private int page;

    private int size;

    private int totalPage ;

    private int numberRecordPage;

    private long totalRecord;

    private T data ;

    public FormPaging() {
    }

    public FormPaging(int page, int size, int totalPage, int numberRecordPage, long totalRecord, T data) {
        this.page = page;
        this.size = size;
        this.numberRecordPage = numberRecordPage;
        this.data = data;
        this.totalPage = totalPage;
        this.totalRecord = totalRecord;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public long getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(long totalRecord) {
        this.totalRecord = totalRecord;
    }

    public int getNumberRecordPage() {
        return numberRecordPage;
    }

    public void setNumberRecordPage(int numberRecordPage) {
        this.numberRecordPage = numberRecordPage;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "FormPaging{" +
                "page=" + page +
                ", size=" + size +
                ", totalPage=" + totalPage +
                ", numberRecordPage=" + numberRecordPage +
                ", totalRecord=" + totalRecord +
                ", data=" + data +
                '}';
    }
}
