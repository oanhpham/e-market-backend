package com.spring.emarket.models.response;

import com.spring.emarket.models.entities.Product;

public class OrderProductDTO {
    ProductDTO product;
    Long quantity;

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
