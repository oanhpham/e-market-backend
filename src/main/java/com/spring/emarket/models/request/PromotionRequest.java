package com.spring.emarket.models.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class PromotionRequest {
    private Long id;

    @NotEmpty(message = "title's campaign is not null ")
    private String title;

    @NotEmpty(message = "content is not null")
    private String content;

    @NotNull(message = "reduce percent is not null")
    private Double reducePercent;

    private Long minTotalMoneyOder;

    private Long limitMoney;

    private String startDate;

    private String endDate;

    private Integer numberDayFollowTimeRegister;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Double getReducePercent() {
        return reducePercent;
    }

    public void setReducePercent(Double reducePercent) {
        this.reducePercent = reducePercent;
    }

    public Long getMinTotalMoneyOder() {
        return minTotalMoneyOder;
    }

    public void setMinTotalMoneyOder(Long minTotalMoneyOder) {
        this.minTotalMoneyOder = minTotalMoneyOder;
    }

    public Long getLimitMoney() {
        return limitMoney;
    }

    public void setLimitMoney(Long limitMoney) {
        this.limitMoney = limitMoney;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getNumberDayFollowTimeRegister() {
        return numberDayFollowTimeRegister;
    }

    public void setNumberDayFollowTimeRegister(Integer numberDayFollowTimeRegister) {
        this.numberDayFollowTimeRegister = numberDayFollowTimeRegister;
    }
}
