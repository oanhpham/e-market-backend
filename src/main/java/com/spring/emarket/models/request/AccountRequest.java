package com.spring.emarket.models.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AccountRequest implements Cloneable {

    private String id;

    private String username;

    private String email;

    private String password;

    @NotNull(message = "phone 's not null")
    private String phone;

    private String avatar;

    @NotNull(message = "address 's not null")
    private String address;

    private Integer status;

    public AccountRequest(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }
    public Object clone() throws
            CloneNotSupportedException
    {
        return super.clone();
    }
}
