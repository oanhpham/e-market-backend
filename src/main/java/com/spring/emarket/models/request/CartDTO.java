package com.spring.emarket.models.request;

import com.spring.emarket.models.entities.OrderProduct;
import com.spring.emarket.models.response.OrderProductDTO;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public class CartDTO {
    @NotNull
    private  Long id;
    @NotNull
    private Long userId;

    @NotNull
    @NotEmpty
    private Double totalPrice = 0.0;

    @NotNull
    @NotEmpty
    private String address;

    @NotNull
    @NotEmpty
    private String telephoneNumber;

    @NotNull
    @NotEmpty
    private Byte payType;

    @NotNull
    @NotEmpty
    private Byte payStatus;

    @NotNull
    @NotEmpty
    private Byte timeToShip;

    private List<OrderProductDTO> orderProducts;

    public List<OrderProductDTO> getOrderProducts() {
        return orderProducts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setOrderProducts(List<OrderProductDTO> orderProducts) {
        this.orderProducts = orderProducts;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public Byte getPayType() {
        return payType;
    }

    public void setPayType(Byte payType) {
        this.payType = payType;
    }

    public Byte getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Byte payStatus) {
        this.payStatus = payStatus;
    }

    public Byte getTimeToShip() {
        return timeToShip;
    }

    public void setTimeToShip(Byte timeToShip) {
        this.timeToShip = timeToShip;
    }
}
