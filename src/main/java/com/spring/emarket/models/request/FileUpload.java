package com.spring.emarket.models.request;

import lombok.Data;

@Data
public class FileUpload {
    private String fileName;
    private String fileDownloadUri;
    private String fileType;
    private long size;
    private Long createdById;

    public FileUpload(String fileName, String fileDownloadUri, String fileType, long size, Long createdById) {
        this.fileName = fileName;
        this.fileDownloadUri = fileDownloadUri;
        this.fileType = fileType;
        this.size = size;
        this.createdById = createdById;
    }
}
