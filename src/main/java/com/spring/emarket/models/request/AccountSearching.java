package com.spring.emarket.models.request;

import lombok.Data;

@Data
public class AccountSearching {

    private String username;
    private String email;
    private String phone;
}
