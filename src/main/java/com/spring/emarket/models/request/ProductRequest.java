package com.spring.emarket.models.request;

import com.spring.emarket.commons.Status;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class ProductRequest {
    private Long id;

    @NotEmpty(message = "product's name is not null")
    private String name;

    private String productCode;

    private String description;

    @NotNull(message = "cost is not null")
    private Integer cost;

    @NotNull(message = "product's category is not null")
    private Long categoryId;

    @NotNull(message = "product's unit is not null")
    private Long unitProductId;

    private Long promotionId;

    private List<String> images;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_ACTIVE;
}
