package com.spring.emarket.models.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class StoreRequest {
    private Long id;
    @NotNull(message = "name is not null")
    private String name;
    @NotNull(message = "address is not null")
    private String address;

    private String imgUrl;
    @NotNull(message = "phoneStoreOwner is not null")
    private String phoneStoreOwner;
    @NotNull(message = "province is not null")
    private Long provinceId;

    @NotNull(message = "districtId is not null")
    private Long districtId;

    @NotNull(message = "wardId is not null")
    private Long wardId;

    @NotNull(message = "street is not null")
    private Long streetId;

    @NotNull(message = "lat is not null")
    private Double lat;
    @NotNull(message = "lng is not null")
    private Double lng;
}
