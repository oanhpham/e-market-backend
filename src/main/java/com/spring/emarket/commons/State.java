package com.spring.emarket.commons;

public enum  State {
    NORMAL(1), BOTH(2), ADMIN(0);

    private final int code;

    State(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static State getByCode(int code) {
        switch (code) {
            case 0:
                return ADMIN;
            case 2:
                return BOTH;
            default:
                return NORMAL;
        }
    }
}
