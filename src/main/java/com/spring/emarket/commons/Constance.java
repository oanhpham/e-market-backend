package com.spring.emarket.commons;

public class Constance {
    final Byte pending = 0;
    final Byte closerTheDeal = 1;
    final Byte goingDelivery = 2;
    final Byte done = 3;

    public Byte getPending() {
        return pending;
    }

    public Byte getCloserTheDeal() {
        return closerTheDeal;
    }

    public Byte getGoingDelivery() {
        return goingDelivery;
    }

    public Byte getDone() {
        return done;
    }
}
