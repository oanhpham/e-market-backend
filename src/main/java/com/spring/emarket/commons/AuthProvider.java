package com.spring.emarket.commons;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}
