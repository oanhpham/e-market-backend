package com.spring.emarket.commons;

public enum  Status {
    NOT_ACTIVE(0), ACTIVE(1), TERMINATE(31);

    private final int code;

    Status(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static Status getByCode(int code) {
        switch (code) {
            case 0:
                return NOT_ACTIVE;
            case 1:
                return ACTIVE;
            default:
                return TERMINATE;
        }
    }
}
