package com.spring.emarket.commons;

public class SystemResponse {

    private int code;

    private String description;

    private Object data;

    public SystemResponse() {
    }

    public SystemResponse(int code, String description, Object data) {
        this.code = code;
        this.description = description;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}

