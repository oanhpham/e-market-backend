package com.spring.emarket.commons;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class StandardizedDateTime {

    public LocalDateTime formatterCustom(String date) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(date, formatter);
    }
}
