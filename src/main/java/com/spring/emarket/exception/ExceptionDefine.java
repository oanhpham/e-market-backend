package com.spring.emarket.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
public class ExceptionDefine extends RuntimeException {
    public int statusCode;
    public String message;
    public Map<String, String> errorObject;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, String> getErrorObject() {
        return errorObject;
    }

    public void setErrorObject(Map<String, String> errorObject) {
        this.errorObject = errorObject;
    }

    public static ForbiddenExceptionDefine forbidden() {
        ForbiddenExceptionDefine forbiddenException = new ForbiddenExceptionDefine();
        forbiddenException.setStatusCode(403);
        return forbiddenException;
    }

    public static UnauthorizedExceptionDefine unauthorized() {
        UnauthorizedExceptionDefine unauthorizedException = new UnauthorizedExceptionDefine();
        unauthorizedException.setStatusCode(401);
        return unauthorizedException;
    }

    public static BadRequestExceptionDefine badRequest(String field, String message) {
        BadRequestExceptionDefine badRequestException = new BadRequestExceptionDefine();
        badRequestException.setStatusCode(400);
        badRequestException.setMessage("bad request");
        Map<String, String> errorObject = new HashMap<>();
        errorObject.put(field, message);
        badRequestException.setErrorObject(errorObject);
        return badRequestException;
    }

    public static NotFoundExceptionDefine notFound() {
        NotFoundExceptionDefine notFoundException = new NotFoundExceptionDefine();
        notFoundException.setStatusCode(404);
        return notFoundException;
    }
}
