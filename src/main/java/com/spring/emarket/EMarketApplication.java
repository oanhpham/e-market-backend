package com.spring.emarket;

import com.spring.emarket.config.AppProperties;
import com.spring.emarket.config.FileStorageProperties;
import com.spring.emarket.services.imps.ProductServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableConfigurationProperties(value = {
        AppProperties.class,
        FileStorageProperties.class
})
public class EMarketApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(EMarketApplication.class, args);
        ProductServiceImpl abc = context.getBean(ProductServiceImpl.class);
//        abc.listProduct("Ca", 0, 0D, 0, 0, 0, 10);
    }

}
