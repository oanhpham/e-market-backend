package com.spring.emarket.services.mapper;

import com.spring.emarket.models.entities.Image;
import com.spring.emarket.models.request.FileUpload;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class FileMapper {

    public Image imageMapper(FileUpload request) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(request, Image.class);
    }


}
