package com.spring.emarket.services.mapper;

import com.spring.emarket.models.entities.*;
import com.spring.emarket.models.response.ProductDTO;
import com.spring.emarket.repositories.*;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class ProductMapper {

    @Autowired
    private CategoryRepository categoryRepo;

    @Autowired
    private PromotionRepository promotionRepo;

    @Autowired
    private UnitProductRepository unitProductRepo;

    @Autowired
    private ProviderRepository providerRepo;

    @Autowired
    private ImageRepository imageRepository;

    public ProductDTO map(Product product){

        ProductDTO productDTO = new ProductDTO();
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(product, productDTO);

        if(product.getCategoryId() > 0){
            Category category = categoryRepo.findById(product.getCategoryId()).orElse(null);
            productDTO.setCategory(category);
        }

        if (product.getPromotionId() != null){
            Promotion promotion = promotionRepo.findById(product.getPromotionId()).orElse(null);
            productDTO.setPromotion(promotion);
        }
        if (product.getProviderId() != null){
            Provider provider = providerRepo.findById(product.getProviderId()).orElse(null);
            productDTO.setProvider(provider);
        }

        if (product.getUnitProductId() != null){
            UnitProduct unitProduct = unitProductRepo.findById(product.getUnitProductId()).orElse(null);
            productDTO.setUnitProduct(unitProduct);
        }
        productDTO.setImages(imageRepository.findByProductId(product.getId()));
//        modelMapper.addMappings(new PropertyMap<Product, ProductDTO>() {
//            protected void configure(){
//                map().set(source.getLinkFace());
//            }
//        });

        return productDTO;
    }


    public Product map(ProductDTO productDTO){
        Product product = new Product();
        ModelMapper modelMapper = new ModelMapper();

        if (productDTO.getPromotion() != null){
            product.setPromotionId(productDTO.getPromotion().getId());
        }

        modelMapper.addMappings(new PropertyMap<ProductDTO, Product>() {
            @Override
            protected void configure() {
                map().setCategoryId(source.getCategory().getId());
                map().setUnitProductId(source.getUnitProduct().getId());
                map().setProviderId(source.getProvider().getId());
            }
        });

        modelMapper.map(productDTO, product);
        product.setStartDate(LocalDateTime.parse(productDTO.getStartDate()));
        product.setExpiryDate(LocalDateTime.parse(productDTO.getExpiryDate()));
        if (productDTO.getId() != null){
            product.setUpdatedAt(LocalDateTime.now());
        }else{
            product.setCreatedAt(LocalDateTime.now());
        }

        return product;
    }

}
