package com.spring.emarket.services;

import com.spring.emarket.models.entities.Cart;
import com.spring.emarket.models.entities.OrderProduct;
import com.spring.emarket.models.request.CartDTO;
import org.springframework.http.ResponseEntity;

public interface OrderService {

    ResponseEntity<?> initial(Long userId, Cart order);

    ResponseEntity<?> saveOrderProduct(Long userId, OrderProduct orderProduct);

    ResponseEntity<?> removeProductFromCart(Long userId, OrderProduct orderProduct);

    ResponseEntity<?> updateCart(Long userId, CartDTO cartDTO);

    ResponseEntity<?> getDetailCart(Long userId);

    ResponseEntity<?> processingOrder(Long userId);

    ResponseEntity<?> shippingMode(Long userId, Long id);

    ResponseEntity<?> cancelMode(Long userId, Long id);

    ResponseEntity<?> doneMode(Long userId, Long id);

    ResponseEntity<?> getHistoryCart(Long userId);

    ResponseEntity<?> getOrderProductIsDelete (Long userId);

    ResponseEntity<?> totalTransactionInWeek ();

    ResponseEntity<?> totalTransactionInMonth ();

    ResponseEntity<?> ratioTransaction ();

    ResponseEntity<?> getAllCartInProcess();

    ResponseEntity<?> getAllCartShipping();

    ResponseEntity<?> getAllCartDone();

    ResponseEntity<?> cancelMode();

}
