package com.spring.emarket.services;

import com.spring.emarket.models.entities.UnitProduct;

import java.util.List;

public interface UnitProductService {

    UnitProduct create (UnitProduct unitProduct);
    UnitProduct update (UnitProduct unitProduct);
    List<UnitProduct> units();
}
