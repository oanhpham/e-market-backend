package com.spring.emarket.services;

import com.spring.emarket.models.entities.Category;

import java.util.List;

public interface CategoryService {

    Category create(Category category);

    Category update(Long categoryId, Category category);

    List<Category> getAllCategories();

    Category deleteCategory(Long id);
}
