package com.spring.emarket.services.imps;

import com.spring.emarket.commons.Status;
import com.spring.emarket.exception.ResourceNotFoundException;
import com.spring.emarket.models.entities.Cart;
import com.spring.emarket.models.entities.OrderProduct;
import com.spring.emarket.models.entities.Product;
import com.spring.emarket.models.request.CartDTO;
import com.spring.emarket.models.response.OrderProductDTO;
import com.spring.emarket.models.response.ProductDTO;
import com.spring.emarket.repositories.OrderProductRepository;
import com.spring.emarket.repositories.OrderRepository;
import com.spring.emarket.repositories.ProductRepository;
import com.spring.emarket.repositories.UserRepository;
import com.spring.emarket.services.OrderService;
import com.spring.emarket.services.mapper.ProductMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
class CartServiceImpls implements OrderService {

    @Autowired
    OrderRepository repository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    OrderProductRepository orderProductRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProductMapper productMapper;

    final Byte isDelete = 0;
    final Byte delete = 1;

    public ResponseEntity<?> initial(Long userId, Cart order) {
        if (userId == null) {
            return new ResponseEntity<>("userId was missing", HttpStatus.BAD_REQUEST);
        }
        Cart cart = repository.findByUserIdAndIsDeleted(userId, isDelete);
        if (cart != null) {
            order.setId(cart.getId());
        }
        return new ResponseEntity<>(repository.save(order), HttpStatus.OK);
    }

    public ResponseEntity<?> saveOrderProduct(Long userId, OrderProduct orderProduct) {

        Product product = productRepository.findById(orderProduct.getProductId()).orElse(null);
        if (product == null || product.getQuantity() < 1) {
            return new ResponseEntity<>("san pham nay tam thoi het hang", HttpStatus.BAD_REQUEST);
        }
        userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("Product", "id", orderProduct.getProductId()));
        Cart cart = repository.findByUserIdAndIsDeleted(userId, isDelete);
        if (cart == null) {
            cart = new Cart();
            cart.setUserId(userId);
            initial(userId, cart);
        }
        ProductDTO productDTO = productMapper.map(product);
        orderProduct.setOrderId(cart.getId());
        OrderProduct orderProductExist = orderProductRepository.findByOrderIdAndProductIdAndIsDelete(orderProduct.getOrderId(), orderProduct.getProductId(), (byte) 0);
        if (orderProductExist != null) {
            cart.setTotalPrice(productDTO.getPromotion() == null ? cart.getTotalPrice() + orderProduct.getQuantity() * productDTO.getCost() : cart.getTotalPrice() + orderProduct.getQuantity()*productDTO.getCost()*(1-productDTO.getPromotion().getReducePercent()));
            Long quantity = orderProductExist.getQuantity() + orderProduct.getQuantity();
            orderProduct.setQuantity(quantity);
            orderProduct.setId(orderProductExist.getId());
            orderProduct.setCreatedDate(LocalDateTime.now());
        } else {
            cart.setTotalPrice(productDTO.getPromotion() == null ? cart.getTotalPrice() + orderProduct.getQuantity()*productDTO.getCost() : cart.getTotalPrice() + orderProduct.getQuantity()*productDTO.getCost()*(1-productDTO.getPromotion().getReducePercent()));
        }
        orderProduct.setUserId(userId);
        repository.save(cart);
        return new ResponseEntity<>(orderProductRepository.save(orderProduct), HttpStatus.OK);

    }

    public ResponseEntity<?> removeProductFromCart (Long userId, OrderProduct orderProduct) {
        userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User", "id", orderProduct.getUserId()));
        Cart cart = repository.findByUserIdAndIsDeleted(userId, isDelete);
        if (cart == null) {
            return new ResponseEntity<>("cart hasn't found", HttpStatus.BAD_REQUEST);
        }
        ProductDTO productDTO = productMapper.map(productRepository.findById(orderProduct.getProductId()).orElseThrow(() -> new ResourceNotFoundException("Product", "id", orderProduct.getProductId())));
        OrderProduct orderProductExist = orderProductRepository.findByUserIdAndProductIdAndIsDelete(userId, orderProduct.getProductId(), (byte) 0);
        if (orderProductExist != null) {
            cart.setTotalPrice(productDTO.getPromotion() == null ? cart.getTotalPrice() - orderProduct.getQuantity() * productDTO.getCost() : cart.getTotalPrice() - orderProduct.getQuantity() * productDTO.getCost()* cart.getTotalPrice() - orderProduct.getQuantity() * productDTO.getCost()*(1-productDTO.getPromotion().getReducePercent()));
            Long quantity = orderProductExist.getQuantity() - orderProduct.getQuantity();
            if (quantity == 0) {
                orderProduct.setIsDelete((byte)1);
            } else {
                orderProduct.setQuantity(quantity);
            }
            orderProduct.setId(orderProductExist.getId());
            orderProduct.setOrderId(orderProductExist.getOrderId());
            orderProduct.setUserId(userId);
            orderProduct.setCreatedDate(LocalDateTime.now());
            repository.save(cart);
        }
        return new ResponseEntity<>(orderProductRepository.save(orderProduct), HttpStatus.OK);
    }

    public ResponseEntity<?> updateCart(Long userId, CartDTO cartDTO) {
        Byte delete = 1;
        Cart cart = repository.findByUserIdAndIsDeleted(userId, isDelete);
        cart.setAddress(cartDTO.getAddress());
        cart.setTelephoneNumber(cartDTO.getTelephoneNumber());
        List<OrderProduct> orderProducts = orderProductRepository.findByOrderIdAndIsDelete(cart.getId(), isDelete);
        orderProducts.forEach(order -> {
            order.setIsDelete(delete);
            orderProductRepository.save(order);
        });

        return new ResponseEntity<>(repository.save(cart), HttpStatus.OK);
    }

    public ResponseEntity<?> getDetailCart(Long userId) {
        ModelMapper modelMapper = new ModelMapper();
        Cart cart = repository.findByUserIdAndIsDeleted(userId, isDelete);
        if (cart == null) {
            cart = new Cart();
            cart.setUserId(userId);
            repository.save(cart);
        }
        CartDTO cartDTO = modelMapper.map(cart, CartDTO.class);
        List<OrderProduct> orderProducts = orderProductRepository.findByUserIdAndIsDelete(userId, isDelete);
        List<OrderProductDTO> products = new ArrayList<>();
        orderProducts.forEach(order -> {
            OrderProductDTO dto = new OrderProductDTO();
            dto.setQuantity(order.getQuantity());
            ProductDTO product = productMapper.map(productRepository.findById(order.getProductId()).orElseThrow(() -> new ResourceNotFoundException("product", "id", "not found")));
            if (product != null) {
                dto.setProduct(product);
            }
            products.add(dto);
            cartDTO.setOrderProducts(products);
        });
        return new ResponseEntity<>(cartDTO, HttpStatus.OK);
    }

    public ResponseEntity<?> processingOrder(Long userId) {
        Cart cart = repository.findByUserIdAndIsDeleted(userId, isDelete);
        if (cart == null) {
            return new ResponseEntity<>("user" + userId.toString(), HttpStatus.FORBIDDEN);
        }
        cart.setPayStatus((byte)1);
        return new ResponseEntity<>(repository.save(cart), HttpStatus.OK);
    }

    public ResponseEntity<?> shippingMode(Long userId, Long id) {
        Cart cart = repository.findById(id).orElse(null);
        if (cart == null) {
            return new ResponseEntity<>("user" + userId.toString(), HttpStatus.FORBIDDEN);
        }
        List<OrderProduct> orderProductList = orderProductRepository.findByOrderIdAndIsDelete(id, delete);
        orderProductList.forEach(order -> {
            Product product = productRepository.findByIdAndStatus(order.getProductId(), Status.ACTIVE);
            if (product != null) {
                product.setQuantity((int) (product.getQuantity() - order.getQuantity()));
                productRepository.save(product);
            }
        });
        cart.setPayStatus((byte)2);
        cart.setIsDeleted((byte)1);
        return new ResponseEntity<>(repository.save(cart), HttpStatus.OK);
    }

    public ResponseEntity<?> cancelMode(Long userId, Long id) {
        Cart cart = repository.findById(id).orElse(null);
        if (cart == null) {
            return new ResponseEntity<>("user" + userId.toString(), HttpStatus.FORBIDDEN);
        }
        if(cart.getPayStatus() == 2) {
            return new ResponseEntity<>("You can't cancel this order", HttpStatus.METHOD_NOT_ALLOWED);
        }
        List<OrderProduct> orderProductList = orderProductRepository.findByOrderIdAndIsDelete(id, delete);
        orderProductList.forEach(order -> {
            Product product = productRepository.findByIdAndStatus(order.getProductId(), Status.ACTIVE);
            if (product != null) {
                product.setQuantity((int) (product.getQuantity() + order.getQuantity()));
                productRepository.save(product);
            }
        });
        cart.setPayStatus((byte)3);
        cart.setIsDeleted((byte)1);
        return new ResponseEntity<>(repository.save(cart), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> doneMode(Long userId, Long id) {
        Cart cart = repository.findCartByPayStatusAndUserId((byte)2, userId);
        if (cart == null) {
            return new ResponseEntity<>("Your's cart still in processing mode", HttpStatus.ACCEPTED);
        }
        cart.setPayStatus((byte)4);
        cart.setIsDeleted((byte)1);
        return new ResponseEntity<>(repository.save(cart), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> getHistoryCart(Long userId) {
        ModelMapper modelMapper = new ModelMapper();
        List<Cart> cartHistory = repository.findCartHistory(userId);
        if (cartHistory.size() == 0) {
            return new ResponseEntity<>(null, HttpStatus.ACCEPTED);
        }
        List<CartDTO> cartDTOList = new ArrayList<>();
        cartHistory.forEach(cart -> {
            CartDTO cartDTO = modelMapper.map(cart, CartDTO.class);
            List<OrderProduct> orderProducts = orderProductRepository.findByOrderIdAndIsDelete(cart.getId(), delete);
            List<OrderProductDTO> products = new ArrayList<>();
            orderProducts.forEach(order -> {
                OrderProductDTO dto = new OrderProductDTO();
                dto.setQuantity(order.getQuantity());
                ProductDTO product = productMapper.map(productRepository.findById(order.getProductId()).orElseThrow(() -> new ResourceNotFoundException("product", "id", "not found")));
                if (product != null) {
                    dto.setProduct(product);
                }
                products.add(dto);
                cartDTO.setOrderProducts(products);
            });
            cartDTOList.add(cartDTO);
        });
        return new ResponseEntity<>(cartDTOList, HttpStatus.OK);
    }

    public ResponseEntity<?> getOrderProductIsDelete (Long userId) {
        return new ResponseEntity<>(orderProductRepository.findByUserIdAndIsDelete(userId, (byte)1), HttpStatus.OK);
    }

    public ResponseEntity<?> totalTransactionInWeek() {
        return new ResponseEntity<>(repository.countCartInWeek(), HttpStatus.OK);
    }
      public ResponseEntity<?> totalTransactionInMonth() {
        return new ResponseEntity<>(repository.countCartInMonth(), HttpStatus.OK);
    }
    public ResponseEntity<?> ratioTransaction () {
        Byte cancel = 3;
        Byte done = 4;
        Long cancellationCart = repository.countCartByPayStatus(cancel);
        Long successCart = repository.countCartByPayStatus(done);
        Double ratio = ((double) successCart/(successCart + cancellationCart)) * 100;
        return new ResponseEntity<>(ratio.toString().length() > 5 ? ratio.toString().substring(0,5) + '%' : ratio.toString() + '%', HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> getAllCartInProcess() {
        Byte processingMode = 1;
        return new ResponseEntity<>(repository.findAllByPayStatus(processingMode), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> getAllCartShipping() {
        ModelMapper modelMapper = new ModelMapper();
        Byte shippingMode = 2;
        List<CartDTO> cartDTOList = new ArrayList<>();
        repository.findAllByPayStatus(shippingMode).forEach(cart -> {
            CartDTO cartDTO = modelMapper.map(cart, CartDTO.class);
            List<OrderProduct> orderProducts = orderProductRepository.findByOrderIdAndIsDelete(cart.getId(), delete);
            List<OrderProductDTO> products = new ArrayList<>();
            orderProducts.forEach(order -> {
                OrderProductDTO dto = new OrderProductDTO();
                dto.setQuantity(order.getQuantity());
                ProductDTO product = productMapper.map(productRepository.findById(order.getProductId()).orElseThrow(() -> new ResourceNotFoundException("product", "id", "not found")));
                if (product != null) {
                    dto.setProduct(product);
                }
                products.add(dto);
                cartDTO.setOrderProducts(products);
            });
            cartDTOList.add(cartDTO);
        });
        return new ResponseEntity<>(cartDTOList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> getAllCartDone() {
        Byte doneMode = 4;
        return new ResponseEntity<>(repository.findAllByPayStatus(doneMode), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> cancelMode() {
        Byte cancelMode = 3;
        return new ResponseEntity<>(repository.findAllByPayStatus(cancelMode), HttpStatus.OK);
    }
}
