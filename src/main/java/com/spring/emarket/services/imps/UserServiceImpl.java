package com.spring.emarket.services.imps;

import com.spring.emarket.commons.Status;
import com.spring.emarket.exception.ResourceNotFoundException;
import com.spring.emarket.models.entities.User;
import com.spring.emarket.models.request.UserRequest;
import com.spring.emarket.repositories.UserRepository;
import com.spring.emarket.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;

    @Override
    public User updateInfo(Long userId, UserRequest userRequest) {
        User user = userRepository.findById(userId).orElseThrow(
                () -> new ResourceNotFoundException("User", "id", userId));
        user.setName(userRequest.getName().equals("")? user.getName(): userRequest.getName());
        user.setFullName(userRequest.getFullName().equals("")? user.getFullName(): userRequest.getFullName());
        user.setAddress(userRequest.getAddress().equals("")? user.getAddress(): userRequest.getAddress());
        user.setPhone(userRequest.getPhone().equals("")? user.getPhone(): userRequest.getPhone());
        return userRepository.save(user);
    }

    @Override
    public User userInfo(Long userId) {
        return userRepository.findById(userId).orElseThrow(
                () -> new ResourceNotFoundException("User", "id", userId));
    }

    @Override
    public Boolean delete(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));
        if (user != null) {
            user.setStatus(Status.TERMINATE);
            userRepository.save(user);
            return true;
        }
        return false;
    }

    @Override
    public Long totalUser() {
        Status status = Status.ACTIVE;
        return userRepository.countUserByStatus(status);
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.getAllByStatus(Status.ACTIVE);
    }
}
