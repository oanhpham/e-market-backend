package com.spring.emarket.services.imps;

import com.spring.emarket.commons.Status;
import com.spring.emarket.models.entities.Promotion;
import com.spring.emarket.models.request.PromotionRequest;
import com.spring.emarket.repositories.PromotionRepository;
import com.spring.emarket.repositories.UserRepository;
import com.spring.emarket.services.PromotionService;
import com.spring.emarket.services.validate.StoreValidator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class PromotionServiceImpl implements PromotionService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    PromotionRepository promotionRepository;
    @Autowired
    StoreValidator validator;

    @Override
    public Promotion create(Long userId, PromotionRequest promotionRequest) {
        if (userRepository.findById(userId).isPresent()) {
            LocalDateTime startDate = formatterDate(promotionRequest.getStartDate());
            LocalDateTime endDate = formatterDate(promotionRequest.getEndDate());
            Promotion promotion = mapper(promotionRequest);
            promotion.setStartDate(startDate);
            promotion.setEndDate(endDate);
            promotionRepository.save(promotion);
            return promotion;
        }
        return null;
    }

    @Override
    public Promotion update(Long userId, Long promotionId, PromotionRequest promotion) {
        if (validator.owner(userId)) {
            Promotion promotionUpdate = promotionRepository.findById(promotionId).orElse(null);
            if (promotionUpdate != null) {
                promotionUpdate = mapper(promotion);
                promotionUpdate.setStartDate(formatterDate(promotion.getStartDate()));
                promotionUpdate.setEndDate(formatterDate(promotion.getEndDate()));
                promotionRepository.save(promotionUpdate);
                return promotionUpdate;
            }
        }
        return null;
    }

    @Override
    public List<Promotion> listPromotion() {
        return promotionRepository.findAll();
    }

    @Override
    public Promotion delete(Long userId, Long promotionId) {
        if (validator.owner(userId)) {
            Promotion promotion = promotionRepository.findById(promotionId).orElse(null);
            if (promotion != null) {
                promotion.setIsDelete((byte) 1);
                promotionRepository.save(promotion);
            }
            return promotion;
        }
        return null;
    }

    private Promotion mapper(PromotionRequest promotionRequest) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(promotionRequest, Promotion.class);
    }

    private LocalDateTime formatterDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(date, formatter);
    }
}
