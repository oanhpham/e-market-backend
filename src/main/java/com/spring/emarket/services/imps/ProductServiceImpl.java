package com.spring.emarket.services.imps;

import com.spring.emarket.commons.DataUtil;
import com.spring.emarket.commons.Role;
import com.spring.emarket.commons.Status;
import com.spring.emarket.exception.ExceptionDefine;
import com.spring.emarket.exception.ResourceNotFoundException;
import com.spring.emarket.models.entities.Image;
import com.spring.emarket.models.entities.Product;
import com.spring.emarket.models.entities.User;
import com.spring.emarket.models.response.FormPaging;
import com.spring.emarket.models.response.ProductDTO;
import com.spring.emarket.repositories.ImageRepository;
import com.spring.emarket.repositories.ProductRepository;
import com.spring.emarket.repositories.UserRepository;
import com.spring.emarket.repositories.dao.ProductDao;
import com.spring.emarket.security.UserPrincipal;
import com.spring.emarket.services.ProductService;
import com.spring.emarket.services.mapper.ProductMapper;
import com.spring.emarket.services.validate.ProductValidate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private Logger logger = LogManager.getLogger(ProductServiceImpl.class);
    @Autowired
    ProductRepository repository;
    @Autowired
    ImageRepository imageRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ProductMapper mapper;
    @Autowired
    private ProductDao productDao;
    @Autowired
    private ProductValidate validate;
    @Autowired
    private ProductRepository productRepository;


    @Override
    public ResponseEntity<?> create(ProductDTO productDTO, UserPrincipal userPrincipal) {

        User owner = userRepository.findByEmail(userPrincipal.getEmail())
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId()));

        if (owner.getRole() != Role.ADMIN) {
            throw ExceptionDefine.badRequest(owner.toString(), "permission denied");
        }
        ResponseEntity<?> valid = validate.validateCreate(productDTO);
        if (valid.getStatusCode().is4xxClientError()) {
            return valid;
        }

        // save
        Product product = repository.save(mapper.map(productDTO));
        logger.info("Inserted !, return : " + product);

        List<Image> images = productDTO.getImages();
        saveImage(product.getId(), images, userPrincipal);

        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> update(ProductDTO productDTO, UserPrincipal userPrincipal) {

        User owner = userRepository.findByEmail(userPrincipal.getEmail())
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId()));

        if (owner.getRole() != Role.ADMIN) {
            throw ExceptionDefine.badRequest(owner.toString(), "permission denied");
        }
        Product product = repository.findByIdAndStatus(productDTO.getId(), Status.ACTIVE);
        if (product == null) {
            logger.error("There is not exist database, recheck id !");
            return new ResponseEntity<>("There is not exist database, recheck id !", HttpStatus.BAD_REQUEST);
        }

        ResponseEntity<?> valid = validate.validateCreate(productDTO);
        if (valid.getStatusCode().is4xxClientError()) {
            return valid;
        }

        // update
        Product product1 = repository.save(mapper.map(productDTO));
        logger.info("Updated !, data: " + product1);

        List<Image> images = productDTO.getImages();

        // delete relationship with productId and save
        imageRepository.deleteRelationshipProduct(productDTO.getId());
        saveImage(productDTO.getId(), images, userPrincipal);

        return new ResponseEntity<>(product1, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> delete(Long id) {
        if (repository.findByIdAndStatus(id, Status.ACTIVE) == null) {
                return new ResponseEntity<>("Invalid " + id, HttpStatus.BAD_REQUEST);
        }
        repository.deleteById(id);
        logger.info("Delete id !" + id);
        return new ResponseEntity<>("Delete ids !" + id, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> listProduct(String name, int id, Double costStart, Double costEnd,
                                         int categoryId, int promotionId, int page, int size) {
        // filter
        FormPaging<?> result = productDao.filter(name, id, costStart, costEnd, categoryId, promotionId, page, size);

        logger.info("Success !, data: " + result);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> rate(long idProduct, int star) {
        if (idProduct <= 0 && star <= 0) {
            logger.error("Data invalid !");
            return new ResponseEntity<>("Data invalid !", HttpStatus.BAD_REQUEST);
        }

        Product product = repository.findByIdAndStatus(idProduct, Status.ACTIVE);
        if (product == null) {
            return new ResponseEntity<>("There is no record in database with idProduct = " + idProduct + " !", HttpStatus.BAD_REQUEST);
        }

        switch (star) {
            case 1:
                product.setstar(product.getstar() + 1);
                break;
            case 2:
                product.setstar2(product.getstar2() + 1);
                break;
            case 3:
                product.setstar3(product.getstar3() + 1);
                break;
            case 4:
                product.setstar4(product.getstar4() + 1);
                break;
            case 5:
                product.setstar5(product.getstar5() + 1);
                break;
            default:
                logger.error("Star invalid !");
                return new ResponseEntity<>("Star invalid !", HttpStatus.BAD_REQUEST);
        }

        // save
        repository.save(product);
        logger.info("Success !, rated ");

        return new ResponseEntity<>("Success, rated !", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<?> getDetailProduct(Long id) {
        ProductDTO productDTO = mapper.map(productRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("product", "id", "not found")));
        return new ResponseEntity<>(productDTO, HttpStatus.OK);
    }

    private void saveImage(Long idProduct, List<Image> images, UserPrincipal userPrincipal) {
        if (DataUtil.isNullOrEmpty(images)) {
            return;
        }

        images.forEach(it -> {
            it.setProductId(idProduct);
            it.setCreatedAt(LocalDateTime.now());
            imageRepository.save(it);
        });
        logger.info("Updated image in database for product id " + idProduct);
    }

    @Override
    public ResponseEntity<?> getArrivalsProduct() {
        return new ResponseEntity<>(repository.getProducts().stream().map(it -> mapper.map(it)), HttpStatus.OK);
    }
    LocalDateTime parseDate (String date) {
        return LocalDateTime.parse(date);
    }

}
