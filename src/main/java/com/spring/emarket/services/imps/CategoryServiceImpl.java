package com.spring.emarket.services.imps;

import com.spring.emarket.commons.Status;
import com.spring.emarket.exception.ExceptionDefine;
import com.spring.emarket.models.entities.Category;
import com.spring.emarket.repositories.CategoryRepository;
import com.spring.emarket.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public Category create(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public Category update(Long categoryId, Category category) {
        if (categoryRepository.findById(categoryId).isPresent()) {
            return categoryRepository.save(category);
        }
        throw ExceptionDefine.badRequest(category.toString(), "the category not found in storage");
    }

    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public Category deleteCategory(Long id) {
        Category category = categoryRepository.findById(id).orElse(null);
        if (category != null) {
            category.setStatus(Status.TERMINATE);
            return categoryRepository.save(category);
        }
        throw ExceptionDefine.badRequest(category.toString(), "the category not found in storage");
    }
}
