package com.spring.emarket.services.imps;

import com.spring.emarket.exception.ExceptionDefine;
import com.spring.emarket.models.entities.UnitProduct;
import com.spring.emarket.repositories.UnitProductRepository;
import com.spring.emarket.services.UnitProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnitProductProductServiceImpls implements UnitProductService {
    @Autowired
    UnitProductRepository repository;

    @Override
    public UnitProduct create(UnitProduct unitProduct) {
        return repository.save(unitProduct);
    }

    @Override
    public UnitProduct update(UnitProduct unitProduct) {
        if(repository.findById(unitProduct.getId()).isPresent()) {
            repository.save(unitProduct);
            return unitProduct;
        }
        throw ExceptionDefine.badRequest(unitProduct.toString(), "unit product to updating not found");
    }

    @Override
    public List<UnitProduct> units() {
        return repository.findAll();
    }
}
