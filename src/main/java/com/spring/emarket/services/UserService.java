package com.spring.emarket.services;

import com.spring.emarket.commons.Status;
import com.spring.emarket.models.entities.User;
import com.spring.emarket.models.request.UserRequest;

import java.util.List;

public interface UserService {

    User updateInfo(Long userId, UserRequest userRequest);
    User userInfo(Long userId);
    Boolean delete(Long userId);
    Long totalUser();
    List<User> getAllUser();
}
