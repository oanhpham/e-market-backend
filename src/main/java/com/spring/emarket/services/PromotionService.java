package com.spring.emarket.services;

import com.spring.emarket.models.entities.Promotion;
import com.spring.emarket.models.request.PromotionRequest;

import java.util.List;

public interface PromotionService {

    Promotion create(Long userId, PromotionRequest promotion);

    Promotion update(Long userId, Long promotionId, PromotionRequest promotion);

    List<Promotion> listPromotion();

    Promotion delete(Long userId, Long promotionId);
}
