package com.spring.emarket.services;

import com.spring.emarket.models.request.FileUpload;
import com.spring.emarket.security.UserPrincipal;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

public interface ImageService {

    FileUpload storeFile(UserPrincipal userPrincipal, MultipartFile file);

    Resource loadFileAsResource(UserPrincipal userPrincipal,String nameImage);
}
