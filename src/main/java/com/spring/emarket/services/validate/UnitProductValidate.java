package com.spring.emarket.services.validate;

import com.spring.emarket.models.entities.UnitProduct;
import com.spring.emarket.repositories.UnitProductRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UnitProductValidate {

    private Logger logger = LogManager.getLogger(UnitProductValidate.class);

    @Autowired
    private UnitProductRepository unitProductRepo;

    public boolean checkExistDB(Long id){
        UnitProduct unitProduct = unitProductRepo.findById(id).orElse(null);
        if (unitProduct == null){
            logger.error("Unit Product is not exist database !");
            return false;
        }
        return true;
    }


}
