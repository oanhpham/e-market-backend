package com.spring.emarket.services.validate;

import com.spring.emarket.models.entities.Provider;
import com.spring.emarket.repositories.ProviderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProviderValidate {
    @Autowired
    ProviderRepository providerRepository;

    boolean checkProviderExist(Long providerId) {
        Provider provider = providerRepository.findProviderByIdAndIsDelete(providerId, (byte)0);
        if (provider == null) {
            return false;
        }
        return true;
    }
}
