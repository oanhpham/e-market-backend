package com.spring.emarket.services.validate;

import com.spring.emarket.commons.Status;
import com.spring.emarket.models.entities.Category;
import com.spring.emarket.repositories.CategoryRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CategoryValidate {

    private Logger logger = LogManager.getLogger(CategoryValidate.class);

    @Autowired
    private CategoryRepository categoryRepo;

    public boolean checkExistDB(Long idCategory){
        Category category = categoryRepo.findByIdAndStatus(idCategory, Status.ACTIVE);
        if (category == null){
            logger.error("Category is not exist database !");
            return false;
        }
        return true;
    }
}
