package com.spring.emarket.services.validate;

import com.spring.emarket.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StoreValidator {
    @Autowired
    UserRepository userRepository;

     public Boolean owner(Long id) {
        if (!userRepository.findById(id).isPresent()) {
            return false;
        }
        return true;
    }
}
