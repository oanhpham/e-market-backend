package com.spring.emarket.services.validate;

import com.spring.emarket.models.entities.Promotion;
import com.spring.emarket.models.entities.Provider;
import com.spring.emarket.models.response.ProductDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ProductValidate {

    private Logger logger = LogManager.getLogger(ProductValidate.class);

    @Autowired
    private CategoryValidate categoryValidate;

    @Autowired
    private UnitProductValidate unitProductValidate;

    @Autowired
    private PromotionValidate promotionValidate;

    @Autowired
    private ProviderValidate providerValidate;
    /**
     * Validate for create product
     *
     * @param productDTO
     *
     *
     * @return ResponseEntity</?>
     * */
    public ResponseEntity<?> validateCreate(ProductDTO productDTO){

        // validate category
        long idCategory = productDTO.getCategory().getId();
        if (idCategory <= 0){
            logger.error("There is no id category !");
            return new ResponseEntity<>("There is no id category !", HttpStatus.BAD_REQUEST);
        }

        if (!categoryValidate.checkExistDB(idCategory)){
            return new ResponseEntity<>("There is no id category !", HttpStatus.BAD_REQUEST);
        }


        // validate unit product
        long idUnit = productDTO.getUnitProduct().getId();
        if (idUnit <= 0){
            logger.error("There is no id category !");
            return new ResponseEntity<>("There is no id unit product !", HttpStatus.BAD_REQUEST);
        }

        if (!unitProductValidate.checkExistDB(idUnit)){
            return new ResponseEntity<>("There is no id unit product !", HttpStatus.BAD_REQUEST);
        }

        Provider provider = productDTO.getProvider();
        if (provider == null) {
            return new ResponseEntity<>("there is'nt provider", HttpStatus.BAD_REQUEST);
        } else {
            if (!providerValidate.checkProviderExist(provider.getId())) {
                return new ResponseEntity<>("there is'nt provider", HttpStatus.BAD_REQUEST);
            }
        }

        // validate promotion
        if(productDTO.getPromotion() != null) {
            Promotion promotion = productDTO.getPromotion();
            if (promotion == null){
                logger.error("There is no id category !");
                return new ResponseEntity<>("There is no id unit product !", HttpStatus.BAD_REQUEST);
            }
            if (!promotionValidate.checkExistDB(productDTO.getPromotion().getId())){
                return new ResponseEntity<>("There is no id promotion !", HttpStatus.BAD_REQUEST);
            }
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }


}
