package com.spring.emarket.services.validate;

import com.spring.emarket.models.request.CartDTO;
import com.spring.emarket.repositories.CategoryRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

public class CartValidate {

    private Logger logger = LogManager.getLogger(ProductValidate.class);

    @Autowired
    private CategoryRepository categoryRepo;

    @Autowired
    private CategoryValidate categoryValidate;

    @Autowired
    private UnitProductValidate unitProductValidate;

    @Autowired
    private PromotionValidate promotionValidate;

}
