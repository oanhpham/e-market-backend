package com.spring.emarket.services.validate;

import com.spring.emarket.models.entities.Promotion;
import com.spring.emarket.repositories.PromotionRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PromotionValidate {

    private Logger logger = LogManager.getLogger(PromotionValidate.class);

    @Autowired
    private PromotionRepository promotionRepo;

    public boolean checkExistDB(Long id){
        Promotion promotion = promotionRepo.findByIdAndIsDelete(id, (byte) 0);
        if (promotion == null){
            logger.error("Promotion is not exist database !");
            return false;
        }
        return true;
    }
}
