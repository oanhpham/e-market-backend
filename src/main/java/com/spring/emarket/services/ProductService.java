package com.spring.emarket.services;

import com.spring.emarket.models.response.ProductDTO;
import com.spring.emarket.security.UserPrincipal;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ProductService {

    ResponseEntity<?> create(ProductDTO productDTO, UserPrincipal userPrincipal);

    ResponseEntity<?> update(ProductDTO productDTO, UserPrincipal userPrincipal);

    ResponseEntity<?> delete(Long id);

    ResponseEntity<?> listProduct(String name, int id, Double costStart, Double costEnd, int categoryId, int promotionId, int page, int size);

    ResponseEntity<?> rate(long idProduct, int star);

    ResponseEntity<?> getDetailProduct(Long id);

    ResponseEntity<?> getArrivalsProduct();

}
